#include "Arduino.h"

//candy detector vars ------------------ start
//phototransistor (IR detector)
#define CANDY_DETECTOR_PIN 0 //A0
//currently observing a candy drop (candy drop triggers this to be true)
boolean observingCandy = false;
//boolean firstRun = true;
int candySensorThreshold = 993; //sensor must read at least this number to detect beam break. Should idle below that.

Thread candyDetectorThresholdThread = Thread();
Thread candyDetectorThread = Thread();
//candy detector vars ------------------ end

void setupCandyDetector(){
    //candy detector setup ------------ start
    pinMode(CANDY_DETECTOR_PIN, INPUT);
    candyDetectorThresholdThread.onRun(updateCandyDetectorThreshold);
    candyDetectorThresholdThread.setInterval(5000);
  
    candyDetectorThread.onRun(checkCandyDetector);
    candyDetectorThread.setInterval(10);

	//delay to give the candy detector time to turn on before taking readings
	delay(2000);
	updateCandyDetectorThreshold();
	
    controller.add(&candyDetectorThresholdThread); 
    controller.add(&candyDetectorThread);
    //candy detector setup ------------ end
}

void updateCandyDetectorThreshold(){
	// String msg;
	int idleValue = analogRead(CANDY_DETECTOR_PIN);
	// msg = String("idleValue: ") + idleValue;
	// putstring_nl(msg);
	// msg = "";
	
	if (!observingCandy){
		int oldValue = candySensorThreshold;
		int newValue = idleValue + 10;
		if (newValue != oldValue){
			candySensorThreshold = newValue;
			// msg = String("Candy detector threshold changed from ") + oldValue + String(" to ") + newValue;
			// Serial.println(msg);
		}else{
			// msg = "Threshold unchanged.";
			// Serial.println(msg);
		}
	}else{
		// msg = "Threshold unchanged because busy observing candy.";
		// Serial.println(msg);
	}
}

void checkCandyDetector(){
  int sensorValue = analogRead(CANDY_DETECTOR_PIN);
  if (sensorValue > candySensorThreshold){
    if (!observingCandy){
      observingCandy = true;
      // String msg = "Candy! ";
      // Serial.println(msg + sensorValue);
	  playSFX(CANDY_DETECTED);
    }
  }
  else{
    observingCandy = false;
  }
}

