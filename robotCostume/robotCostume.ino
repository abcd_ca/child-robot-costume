//ArduinoThread library: https://github.com/ivanseidel/ArduinoThread
#include <Thread.h>
#include <ThreadController.h>

/**
 * pin A0	Candy detector
 * pin A1	Robot boots (right)
 * pin A2	Robot boots (left)
 * pin A3	** unused
 * pin A4	Big Button Pad (MCP23017 port expander)
 * pin A5	Big Button Pad (MCP23017 port expander)
 * --------------------------------------------------------
 * pin D0	** unused
 * pin D1	** unused
 * pin D2	Wave shield (see wavehc library for definition)
 * pin D3	Wave shield (see wavehc library for definition)
 * pin D4	Wave shield (see wavehc library for definition)
 * pin D5	Wave shield (see wavehc library for definition)
 * pin D6	Antennae (left and right in parallel) (pin 6 has PWM)
 * pin D7	Ear Left (yogurt cup)
 * pin D8	Ear Right (yogurt cup)
 * pin D9	** Do not use. (tried with PWM but conflicts for some reason with wave shield when reading a file, halts app)
 * pin D10	Wave shield (see wavehc library for definition)
 * pin D11	Wave shield (see wavehc library for definition)
 * pin D12	Wave shield (see wavehc library for definition)
 * pin D13	Wave shield (see wavehc library for definition)
 */

//constants mapped to soundFile array indicies in _sounds.ino.
#define STARTUP 0
#define BOOT_LEFT 1
#define BOOT_RIGHT 2
#define ZAP 3
#define CANDY_DETECTED 4
#define SERVO1 5
#define TRICK_OR_TREAT 6
#define HAPPY_HALLOWEEN 7
#define BEEP1 8
#define BEEP2 9
#define BEEP3 10
#define BEEP4 11
#define BEEP5 12
#define JETSONS 13
#define R2D2_1 14
#define R2D2_2 15
#define R2D2_3 16
#define TRANSFORMERS 17

ThreadController controller = ThreadController();
	

void setup()
{
	Serial.begin(9600);

	setupLights();
	setupBigButtonPad();
	setupCandyDetector();
	setupBoots();
	setupSFX();
}

void loop()
{
	controller.run();
	// updateAntenna();
}