#include "Arduino.h"

#define fsrBootLeftAnalogPin 2 	// FSR is connected to analog 1
#define fsrBootRightAnalogPin 1	// FSR is connected to analog 2
#define fsrThreshold 100 //value threashold to base state of boot is up/down

boolean leftFootDown = true;
boolean rightFootDown = true;

Thread bootsThread = Thread();

void setupBoots(){
	//FSR pressure sensor (robot boots) setup ------------ end
	bootsThread.onRun(checkBoots);
	bootsThread.setInterval(100);
	controller.add(&bootsThread);
	//FSR pressure sensor (robot boots) setup ------------ end
}

void checkBoots(){
	// Serial.print(F("left boot: ")); Serial.print(analogRead(fsrBootLeftAnalogPin)); Serial.print(F(", right boot: ")); Serial.println(analogRead(fsrBootRightAnalogPin));

	//read the analog reading from the FSR pressure sensor in each boot
	if (!sfxPlaying()){
		if(analogRead(fsrBootLeftAnalogPin) < fsrThreshold){
			// Serial.println(F("left < threshold"));
			if (leftFootDown){
				leftFootDown = false;
				playSFX(BOOT_LEFT);
				Serial.println(F("left foot up"));
			}
		}else{
			leftFootDown = true;
		}
	}
	
	// Serial.println()
	
	if (!sfxPlaying()){
		if(analogRead(fsrBootRightAnalogPin) < fsrThreshold){
			// Serial.println(F("right < threshold"));
			if (rightFootDown){
				rightFootDown = false;
				playSFX(BOOT_RIGHT);
				Serial.println(F("right foot up"));
			}
		}else{
			rightFootDown = true;
		}
	}
}