/**
 * Note, I calculated that forward voltage of each white LED is 3.2mA
 * I put a 200 Ohm resistor (actually 2x100Ohms) on each LED. LEDs are in parallel but share
 * the same digital pin. In total, I measured they draw 16mA combined from the digital pin when at full brightness.
 */
#define ANTENNAE_PIN 6
#define EAR_RIGHT_PIN 8
#define EAR_LEFT_PIN 7

int brightness = 0;
int fadeAmount = 5;
long lightsPreviousMillis = 0;
long lightsInterval = 30;
boolean earToggle = true;


Thread antennaLightsThread = Thread();
Thread earLightsThread = Thread();

void setupLights(){
	antennaLightsThread.onRun(updateAntenna);
	antennaLightsThread.setInterval(30);

	earLightsThread.onRun(updateEars);
	earLightsThread.setInterval(500);
	
	controller.add(&antennaLightsThread);
	controller.add(&earLightsThread);

	pinMode(EAR_LEFT_PIN, OUTPUT);
	pinMode(EAR_RIGHT_PIN, OUTPUT);
}

void updateAntenna(){
	// 	// set the brightness
	analogWrite(ANTENNAE_PIN, brightness);

	// change the brightness for next time through the loop:
	brightness = brightness + fadeAmount;

	// reverse the direction of the fading at the ends of the fade: 
	if (brightness == 0 || brightness == 255) {
	  fadeAmount = -fadeAmount ; 
	}     
}

void updateEars(){
	// Serial.println(F("updateEars"));
	earToggle = !earToggle;
	// Serial.println(earToggle);
	digitalWrite(EAR_RIGHT_PIN, earToggle ? HIGH : LOW);
	digitalWrite(EAR_LEFT_PIN, earToggle ? LOW : HIGH);
}