//based on library example: https://github.com/adafruit/Adafruit-MCP23017-Arduino-Library/blob/master/examples/button/button.pde

// Connect pin #12 of the expander to Analog 5 (i2c clock)
// Connect pin #13 of the expander to Analog 4 (i2c data)
// Connect pins #15, 16 and 17 of the expander to ground (address selection)
// Connect pin #9 of the expander to 5V (power)
// Connect pin #10 of the expander to ground (common ground)

// Input #0 is on pin 21 so connect a button or switch from there to ground

/*
	MCP23017 pins correspond to big button pad pins:

	1	2	3
	4	5	6
	7	8	9
	*	0	#
		F	R

	where F and R are the Flash and Redial buttons respectively. Flash and redial are just two other buttons on the button pad that used to be Flash and Reset in the phone button pad's previous life. They are mapped to pin 10 and 11 respectively.

	MCP23017 pinouts:
	Note, real datasheet pins are here:
	http://ww1.microchip.com/downloads/en/devicedoc/21952b.pdf

	This shows the locations of the 16 pins as they relate to the pin
	number you'd use with mcp.pinMode(n, INPUT).
	These aren't the actual pin numbers given in the data sheet!
	----u----
	8		7
	9		6
	10		5
	11		4
	12		3
	13		2
	14		1
	15		0
	|		|
	|		|
	|		|
	|		|
	|		|
	|		|
	---------
*/

#include "Arduino.h"
#include <Wire.h>
#include "Adafruit_MCP23017.h"
// #include "WaveUtil.h"

int currentBigButton;
int bigButtonMatch = -1;

Thread bigButtonPadThread = Thread();

Adafruit_MCP23017 mcp;

/**
 * these are just 'constants' to refer to each button 
 * on the big button pad without using strings
 */
#define BIG_BUTTON_0 0
#define BIG_BUTTON_1 1
#define BIG_BUTTON_2 2
#define BIG_BUTTON_3 3
#define BIG_BUTTON_4 4
#define BIG_BUTTON_5 5
#define BIG_BUTTON_6 6
#define BIG_BUTTON_7 7
#define BIG_BUTTON_8 8
#define BIG_BUTTON_9 9
#define BIG_BUTTON_Star 10
#define BIG_BUTTON_Pound 11
#define BIG_BUTTON_Flash 12
#define BIG_BUTTON_Redial 12

/**
 * Button pad wire combinations mapped to phone buttons. 
 * Binary representations represent which port expander pins are uniquely LOW when a button
 * is pushed. Serves as a finger print to match the buttons up to pin combinations.
 * Determined through brute force combination testing and looking for uniqueness manually.
 */
#define BIG_BUTTON_0_IO 0b1111111101011111			//with port expander ouput pin 5
#define BIG_BUTTON_1_IO 0b1111110111111110			//with port expander ouput pin 0	
#define BIG_BUTTON_2_IO 0b1111111011111110			//with port expander ouput pin 0
#define BIG_BUTTON_3_IO 0b1111111111101010			//with port expander ouput pin 0
#define BIG_BUTTON_4_IO 0b1111110111111101			//with port expander ouput pin 1
#define BIG_BUTTON_5_IO 0b1111111011111101			//with port expander ouput pin 1
#define BIG_BUTTON_6_IO 0b1111111111101001			//with port expander ouput pin 1
#define BIG_BUTTON_7_IO 0b1111110111110111			//with port expander ouput pin 3
#define BIG_BUTTON_8_IO 0b1111111101110111			//with port expander ouput pin 3
#define BIG_BUTTON_9_IO 0b1111111111100011			//with port expander ouput pin 2
#define BIG_BUTTON_Star_IO 0b1111110111011111		//with port expander ouput pin 5
#define BIG_BUTTON_Pound_IO 0b1111111111001011		//with port expander ouput pin 2
#define BIG_BUTTON_Flash_IO 0b1111111110111101		//with port expander ouput pin 1
//#define BIG_BUTTON_Redial_IO 0b?					//with port expander ouput pin ? // don't have a unique output/input combo for redial. 

void setupBigButtonPad(){
	Serial.println(F("setupBigButtonPad"));
	bigButtonPadThread.onRun(checkButtonPressFoo);
	bigButtonPadThread.setInterval(100);
	
	controller.add(&bigButtonPadThread);
	
	mcp.begin();      // use default address 0
	
	//start with all port expander pins set as inputs
	for(int i = 0; i < 16; i++){
		mcp.pinMode(i, INPUT);
		mcp.pullUp(i, HIGH);  // turn on a 100K pullup internally			
	}
}
	
void checkButtonPressFoo(){
	//IC's 16 pins are all HIGH, thus no buttons are pressed.
	int outputPinsToCheck[] = {0,1,2,3,5};
	int oLen = sizeof(outputPinsToCheck);
	int portExpanderOutPin;

	// Serial.println(mcp.readGPIOAB(), BIN);
	for (int i = 0; i < oLen; i++){
		portExpanderOutPin = outputPinsToCheck[i];
		mcp.pinMode(portExpanderOutPin, OUTPUT);
		mcp.digitalWrite(portExpanderOutPin, LOW);
	
		uint16_t activeWires = mcp.readGPIOAB();
	
		switch (portExpanderOutPin){
			case 0:
				switch(activeWires){
					case BIG_BUTTON_1_IO:
						bigButtonMatch = BIG_BUTTON_1;
						break;
					
					case BIG_BUTTON_2_IO:
						bigButtonMatch = BIG_BUTTON_2;
						break;
					
					case  BIG_BUTTON_3_IO:
						bigButtonMatch = BIG_BUTTON_3;
						break;

					default:
						bigButtonMatch = -1;
						break;
				}
				break;

			case 1:
				switch(activeWires){
					case BIG_BUTTON_4_IO:
						bigButtonMatch = BIG_BUTTON_4;
						break;
				
					case BIG_BUTTON_5_IO:
						bigButtonMatch = BIG_BUTTON_5;
						break;
				
					case  BIG_BUTTON_6_IO:
						bigButtonMatch = BIG_BUTTON_6;
						break;
			
					case  BIG_BUTTON_Flash_IO:
						bigButtonMatch = BIG_BUTTON_Flash;
						break;
					
					default:
						bigButtonMatch = -1;
						break;
				}
				break;
			
			case 2:
				switch(activeWires){
					case BIG_BUTTON_9_IO:
						bigButtonMatch = BIG_BUTTON_9;
						break;
			
					case BIG_BUTTON_Pound_IO:
						bigButtonMatch = BIG_BUTTON_Pound;
						break;
				
					default:
						bigButtonMatch = -1;
						break;
				}
				break;
		
			case 3:
				switch(activeWires){
					case BIG_BUTTON_7_IO:
						bigButtonMatch = BIG_BUTTON_7;
						break;
		
					case BIG_BUTTON_8_IO:
						bigButtonMatch = BIG_BUTTON_8;
						break;
				
					default:
						bigButtonMatch = -1;
						break;
				}
				break;
			
			case 5:
				switch(activeWires){
					case BIG_BUTTON_Star_IO:
						bigButtonMatch = BIG_BUTTON_Star;
						break;
	
					case BIG_BUTTON_0_IO:
						bigButtonMatch = BIG_BUTTON_0;
						break;
									
					default:
						bigButtonMatch = -1;
						break;						
				}
				break;		
		}		
	
		//change output pin back to input pin.
		mcp.pinMode(portExpanderOutPin, INPUT);
		mcp.pullUp(portExpanderOutPin, HIGH);
	
		//stop searching if match found, exit for loop
		if (bigButtonMatch != -1){
			break;
		}
	}	
	
	if (bigButtonMatch == -1){
		//no match, don't do anything, forget currentBigButton
		currentBigButton = -1;
	}else{
		//match found, is it the same one? ignore if same one, otherwise handle new match
		if (bigButtonMatch !=currentBigButton){
			currentBigButton = bigButtonMatch;				
			Serial.print(F("** Button "));
			Serial.print(bigButtonMatch);
			Serial.println(F(" pressed"));

			
			switch (bigButtonMatch){
				case BIG_BUTTON_1:
					playSFX(TRICK_OR_TREAT);
					break;

				case BIG_BUTTON_2:
					playSFX(HAPPY_HALLOWEEN);
					break;
				
				case BIG_BUTTON_3:
					playSFX(SERVO1);
					break;
					
				case BIG_BUTTON_4:
					playSFX(BEEP1);
					break;
			
				case BIG_BUTTON_5:
					playSFX(BEEP2);
					break;
		
				case BIG_BUTTON_6:
					playSFX(BEEP3);
					break;
	
				case BIG_BUTTON_7:
					playSFX(BEEP4);
					break;

				case BIG_BUTTON_8:
					playSFX(BEEP5);
					break;

				case BIG_BUTTON_9:
					playSFX(JETSONS);
					break;

				case BIG_BUTTON_0:
					playSFX(R2D2_1);
					break;

				case BIG_BUTTON_Star:
					playSFX(R2D2_2);
					break;

				case BIG_BUTTON_Pound:
					playSFX(R2D2_3);
					break;

				case BIG_BUTTON_Flash:
					playSFX(TRANSFORMERS);
					break;					
			}
		}
	}
}