//Libraries for wave shield: https://code.google.com/p/wavehc/
#include "Arduino.h"
#include <FatReader.h>
#include <SdReader.h>
#include <avr/pgmspace.h>
#include "WaveHC.h"

/**
 * Credit: Originally based on an Adafruit example from the Wave Shield library but much has changed too.
 */


SdReader card;    // This object holds the information for the card
FatVolume vol;    // This holds the information for the partition on the card
FatReader root;   // This holds the information for the filesystem on the card
FatReader f;      // This holds the information for the file we're play

WaveHC wave;      // This is the only wave (audio) object, since we will only play one at a time
		
int currentSoundFileIndex = 0;

//keeps track of id of the last sound effect playSFX was given. Values defined in main robotCostume file. i.e. 
int lastSoundID;

//sound fx filenames ------ start
prog_char string_0[] PROGMEM = "STARTUP.WAV";   // "String 0" etc are strings to store - change to suit.
prog_char string_1[] PROGMEM = "FOOT_L.WAV";
prog_char string_2[] PROGMEM = "FOOT_R.WAV";
prog_char string_3[] PROGMEM = "ZAP.WAV";
prog_char string_4[] PROGMEM = "THANKYOU.WAV";
prog_char string_5[] PROGMEM = "SERVO1.WAV";
prog_char string_6[] PROGMEM = "TRCKTRT.WAV";
prog_char string_7[] PROGMEM = "HAPPYHLW.WAV";
prog_char string_8[] PROGMEM = "BEEP1.WAV";
prog_char string_9[] PROGMEM = "BEEP2.WAV";
prog_char string_10[] PROGMEM = "BEEP3.WAV";
prog_char string_11[] PROGMEM = "BEEP4.WAV";
prog_char string_12[] PROGMEM = "BEEP5.WAV";
prog_char string_13[] PROGMEM = "JETSONS1.WAV";
prog_char string_14[] PROGMEM = "R2D2.WAV";
prog_char string_15[] PROGMEM = "R2D2B.WAV";
prog_char string_16[] PROGMEM = "R2D2C.WAV";
prog_char string_17[] PROGMEM = "TRNSFRM.WAV";

// Then set up a table to refer to your strings.
PROGMEM const char *wav_filename_string_table[] = 	   
{   
	string_0,
	string_1,
	string_2,
	string_3,
	string_4,
	string_5,
	string_6, 
	string_7,
	string_8,
	string_9,
	string_10,
	string_11,
	string_12,
	string_13,
	string_14,
	string_15,
	string_16,
	string_17,
 };

char filenameBuffer[13];    // make sure this is large enough for the largest string it must hold
// //sound fx filenames ------ end
// 
Thread keepSpeakersOnThread = Thread();

void setupSFX(){
	keepSpeakersOnThread.onRun(onKeepSpeakersOn);
	keepSpeakersOnThread.setInterval(150000); //every 2.5 minutes

	controller.add(&keepSpeakersOnThread);
	
    //sound (wave sheild) setup ------- start
    byte i;
  
    // set up serial port
    Serial.begin(9600);
  
    Serial.print(F("Free RAM: "));       // This can help with debugging, running out of RAM is bad
    Serial.println(getFreeRam());      // if this is under 150 bytes it may spell trouble!)
  
    // Set the output pins for the DAC control. This pins are defined in the library
    pinMode(2, OUTPUT);
    pinMode(3, OUTPUT);
    pinMode(4, OUTPUT);
    pinMode(5, OUTPUT);
 
    // pinMode LED
    pinMode(13, OUTPUT); //TODO inquire if this pin is truly being used by Arduino or if it's just left over from an LED blink test

    //  if (!card.init(true)) { //play with 4 MHz spi if 8MHz isn't working for you
    if (!card.init()) {         //play with 8 MHz spi (default faster!)  
      Serial.println(F("Card init. failed!"));  // Something went wrong, lets print out why
      sdErrorCheck();
      while(1);                            // then 'halt' - do nothing!
    }
  
    // enable optimize read - some cards may timeout. Disable if you're having problems
    card.partialBlockRead(true);
 
  // Now we will look for a FAT partition!
    uint8_t part;
    for (part = 0; part < 5; part++) {     // we have up to 5 slots to look in
      if (vol.init(card, part)) 
        break;                             // we found one, lets bail
    }
    if (part == 5) {                       // if we ended up not finding one  :(
      Serial.println(F("No valid FAT partition!"));
      sdErrorCheck();      // Something went wrong, lets print out why
      while(1);                            // then 'halt' - do nothing!
    }
  
    // Lets tell the user about what we found
    Serial.print(F("Using partition "));
    Serial.print(part, DEC);
    Serial.print(F(", type is FAT"));
    Serial.println(vol.fatType(),DEC);     // FAT16 or FAT32?
  
    // Try to open the root directory
    if (!root.openRoot(vol)) {
      Serial.println(F("Can't open root dir!")); // Something went wrong,
      while(1);                             // then 'halt' - do nothing!
    }
  
    // Whew! We got past the tough parts.
    Serial.println(F("Ready for ELK-390 input!!"));
  
    TCCR2A = 0;
    TCCR2B = 1<<CS22 | 1<<CS21 | 1<<CS20;

    //Timer2 Overflow Interrupt Enable
    // TIMSK2 |= 1<<TOIE2;

	playSFX(STARTUP);
//     //sound (wave sheild) setup ------- end  
}

void playfile(char *name) {
  // see if the wave object is currently doing something
  if (wave.isplaying) {// already playing something, so stop it!
    wave.stop(); // stop it
  }
  // look in the root directory and open the file
  if (!f.open(root, name)) {
    Serial.print(F("Couldn't open file ")); Serial.print(name); return;
  }
  // OK read the file and turn it into a wave object
  if (!wave.create(f)) {
    Serial.println(F("Not a valid WAV")); return;
  }
  
  // ok time to play! start playback
  wave.play();
}

void sdErrorCheck(void)
{
  if (!card.errorCode()) return;
  Serial.print(F("\n\rSD I/O error: "));
  Serial.print(card.errorCode(), HEX);
  Serial.print(F(", "));
  Serial.println(card.errorData(), HEX);
  while(1);
}

/** Return the number of bytes currently free in RAM. */
int getFreeRam(void) {
  extern int  __bss_end;
  extern int  *__brkval;
  int free_memory;
  if((int)__brkval == 0) {
    // if no heap use from end of bss section
    free_memory = ((int)&free_memory) - ((int)&__bss_end);
  }
  else {
    // use from top of stack to heap
    free_memory = ((int)&free_memory) - ((int)__brkval);
  }
  return free_memory;
}

void onKeepSpeakersOn(){
	playSFX(ZAP);
}

void playSFX(int id){
	//play sounds but only if it's not interrupting the startup or candy detector sound that may already be playing.
	//TODO optimize this if/else block, looks a bit redundant
	if (lastSoundID == STARTUP || lastSoundID == CANDY_DETECTED){
		if (!wave.isplaying){
			lastSoundID = id;
			Serial.print(F("play sfx")); Serial.println(id);
			bufferFilename(id);
			playfile(filenameBuffer);
		}
	}else {
		lastSoundID = id;
		Serial.print(F("play sfx")); Serial.println(id);
		bufferFilename(id);
		playfile(filenameBuffer);
	}
}

/**
 * copy filename from progmem into sram filenameBuffer
 */
void bufferFilename(int id){
	strcpy_P(filenameBuffer, (char*)pgm_read_word(&(wav_filename_string_table[id]))); // Necessary casts and dereferencing, just copy. 
}

uint8_t sfxPlaying(){
	wave.isplaying;
}